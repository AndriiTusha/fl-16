import { randomNumber } from '@/js/random'
import { example } from '@/js/example'

export const getDatasetNumber = () => {
  let countWin = 0
  let countLose = 0
  const heading = document.createElement('h1')
  const app = document.querySelector('#root')
  const field = document.getElementById('field')
  field.addEventListener('click', (e) => {
    const rand = randomNumber()
    const clickNumer = +e.target.dataset.number
    if (rand === clickNumer) {
      heading.textContent = example('You win!', countWin)
      app.append(heading)
      countWin++
    } else {
      heading.textContent = example('You loose!', countLose)
      app.append(heading)
      countLose++
    }
    if (countWin === 4 || countLose === 4) {
      const button = document.createElement('button')
      button.innerText = 'Clear'
      app.append(button)
      button.addEventListener('click', (e) => {
        e.preventDefault
        heading.innerText = ''
        countWin = 0
        countLose = 0
        button.remove()
      })
    }
  })
}
