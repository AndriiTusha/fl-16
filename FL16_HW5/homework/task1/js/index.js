let spinner = document.getElementById('spinner');

function getUsers() {
    const url = 'https://jsonplaceholder.typicode.com/users';
    spinner.classList.toggle('hidden');
    return fetch(url, {
        method: 'GET'
    });
}

getUsers().then(
        response => {
            spinner.classList.toggle('hidden');
            return response.json();
        }
    )
    .then(
        users => {
            makeList(users);
        }
    )
    .catch(
        error => {
            console.log(error);
        }
    );


function makeList(users) {
    let root = document.getElementById('root');
    let ol = document.createElement('ol');
    ol.setAttribute('id', 'listRoot');
    root.append(ol);
    let olRoot = document.getElementById('listRoot');
    console.log(users);
    let html = ``;
    let name = ``;
    let username = ``;
    let email = ``;
    let editButton = ``;
    let deleteButton = ``;
    for (let i = 0; i < users.length; i++) {
        name = `<input type="text" value="${users[i]['name']}">`;
        username = `<input type="text" value="${users[i]['username']}">`;
        email = `<input type="email" value="${users[i]['email']}">`;
        editButton = '<input type="button" id="edit" value="EDIT" data-id="' + users[i]['id'] + '">';
        deleteButton = '<input type="button" id="delete" value="DELETE" data-id="' + users[i]['id'] + '">';
        html += `<li>${name} ${username} ${email} ${editButton} ${deleteButton}</li>`;
    }
    olRoot.insertAdjacentHTML('beforeend', html);
    olRoot.addEventListener('click', (e) => {
        if (e.target.getAttribute('id') === 'edit') {
            editList(e);
        }
    });
    olRoot.addEventListener('click', (e) => {
        if (e.target.getAttribute('id') === 'delete') {
            deleteList(e);
        }
    });
}

function editList(element) {
    let elemId = +element.target.dataset.id;
    let nameInputValue = element.target.previousElementSibling.previousElementSibling.previousElementSibling.value;
    let usernameInputValue = element.target.previousElementSibling.previousElementSibling.value;
    let emailInputValue = element.target.previousElementSibling.value;
    let obj = {
        id: elemId,
        name: nameInputValue,
        username: usernameInputValue,
        email: emailInputValue
    };
    let url = `https://jsonplaceholder.typicode.com/posts/${elemId}`;
    spinner.classList.toggle('hidden');
    fetch(url, {
            method: 'PUT',
            body: JSON.stringify(
                obj
            ),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        })
        .then(
            response => {
                spinner.classList.toggle('hidden');
                response.json();
            }
        )
        .then((json) => console.log(json));
}

function deleteList(element) {
    let elemId = +element.target.dataset.id;
    let url = `https://jsonplaceholder.typicode.com/posts/${elemId}`;
    spinner.classList.toggle('hidden');
    fetch(url, {
            method: 'DELETE'
        })
        .then(
            response => {
                spinner.classList.toggle('hidden');
                response.json();
                // spinner.classList.toggle('hidden');
            }
        )
        .then((json) => console.log(json));
}