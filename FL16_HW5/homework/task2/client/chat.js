let root = document.getElementById('root');
let getName = prompt('Enter your name', 'SomeName');
let send = document.getElementById('send');
let button = document.getElementById('button');
let data = new Date();
let timeNow = data.toLocaleTimeString();

let message = '';
button.addEventListener('click', () => {
    console.log(send.value);
    message = send.value;
    socket.onopen(message);
});


let socket = new WebSocket("ws://localhost:8080");

socket.onopen = function(message) {
    // alert("[open] Соединение установлено");
    // alert("Отправляем данные на сервер");
    let sendStr = '<div class="div left"><p>' + getName + '</p><p>' + message + '</p> <p>' + timeNow + '</p></div>';
    root.insertAdjacentHTML('beforeend', sendStr);
    socket.send(sendStr);
};

socket.onmessage = function(event) {
    //alert(`[message] Данные получены с сервера: ${event.data}`);
    //let receiveStr = event.data;
    root.insertAdjacentHTML('beforeend', '<div class="div right"><p>' + event.data + '</p></div>');
};

socket.onclose = function(event) {
    if (event.wasClean) {
        alert(`[close] Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
    } else {
        // например, сервер убил процесс или сеть недоступна
        // обычно в этом случае event.code 1006
        alert('[close] Соединение прервано');
    }
};

socket.onerror = function(error) {
    alert(`[error] ${error.message}`);
};