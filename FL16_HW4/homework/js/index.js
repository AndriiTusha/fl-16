'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */

let ammount = 0;
let ingridientArr = [];
let ingridientArrNames = [];

function Pizza(size, type) {
    this.initialSize = size;
    this.initialType = type;
    this.ingridientArr = ingridientArr;
    this.ammount = ammount;
    this.size = size;
    this.type = type;

    if (this.size === undefined || this.type === undefined) {
        throw new PizzaException('Required two arguments, given: 1');
    }

    // console.log(this.size, this.type);

    if (this.size === 'S') {
        this.size = 50;
    } else if (this.size === 'M') {
        this.size = 75;
    } else if (this.size === 'L') {
        this.size = 100;
    } else {
        throw new PizzaException('invalid size');
    }

    if (this.type === 'VEGGIE') {
        this.type = 50;
    } else if (this.type === 'MARGHERITA') {
        this.type = 75;
    } else if (this.type === 'PEPPERONI') {
        this.type = 100;
    } else {
        throw new PizzaException('invalid type');
    }

    this.ammount = this.size + this.type;


    //console.log(this.size, this.type);

    this.addExtraIngredient = function(ingridient) {
        this.ingridient = ingridient;
        if (this.ingridientArr.indexOf(this.ingridient) > -1) {
            throw new PizzaException('Duplicate ingredient!');
        } else {
            this.ingridientArr.push(this.ingridient);
        }

        if (this.ingridient === 'MEAT' && this.initialType === 'VEGGIE') {
            throw new PizzaException('Invalid ingredient');
        }

        //  console.log(`ingr: ${this.ingridient}. arr: ${this.ingridientArr}`);
        if (this.ingridient === 'TOMATOES') {
            this.ingridient = 5;
        } else if (this.ingridient === 'CHEESE') {
            this.ingridient = 7;
        } else if (this.ingridient === 'MEAT') {
            this.ingridient = 9;
        } else {
            throw new PizzaException('No such ingridients!');
        }
        this.ammount += this.ingridient;
        return this.ammount;
    }

    this.removeExtraIngredient = function(ingridient) {
        this.ingridient = ingridient;
        if (this.ingridientArr.length) {
            if (this.ingridientArr.indexOf(this.ingridient) > -1) {
                this.ingridientArr.splice(this.ingridientArr.indexOf(this.ingridient), 1);
            } else {
                throw new PizzaException('No such ingridient!');
            }
        }
        if (this.ingridient === 'TOMATOES') {
            this.ammount -= 5;
        } else if (this.ingridient === 'CHEESE') {
            this.ammount -= 7;
        } else if (this.ingridient === 'MEAT') {
            this.ammount -= 9;
        } else {
            throw new PizzaException('No such ingridients!');
        }
        return this.ammount;
    };

    this.getExtraIngredients = function() {
        return this.ingridientArr;
    };

    this.getSize = function() {
        return this.initialSize;
    }


    this.getPrice = function() {
        return this.ammount;
    }

    this.getPizzaInfo = function() {

        //this.ingridientArrNames = ingridientArrNames;

        if (this.initialSize === 'S') {
            this.size = 'SMALL';
        } else if (this.initialSize === 'M') {
            this.size = 'MEDIUM';
        } else if (this.initialSize === 'L') {
            this.size = 'LARGE';
        }
        this.part1 = `Your Pizza is: size - ${this.size}; type - ${this.type}; `;
        this.part2 = `extra ingridients - ${this.ingridientArr}; price - ${this.getPrice()} UAH.`;
        this.summary = this.part1 + this.part2;
        return this.summary;
    }
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = 'S';
Pizza.SIZE_M = 'M';
Pizza.SIZE_L = 'L';

Pizza.TYPE_VEGGIE = 'VEGGIE';
Pizza.TYPE_MARGHERITA = 'MARGHERITA';
Pizza.TYPE_PEPPERONI = 'PEPPERONI';

Pizza.EXTRA_TOMATOES = 'TOMATOES';
Pizza.EXTRA_CHEESE = 'CHEESE';
Pizza.EXTRA_MEAT = 'MEAT';

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_PEPPERONI, Pizza.TYPE_MARGHERITA, Pizza.TYPE_VEGGIE];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];


/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */

function PizzaException(message) {
    this.message = message;
    return this.message
}

// Sorry, but I came from my holidays at 9p.m. 5 July and don`t be able to done this hw till deadline. Had to cheating. Done this hw next day completely. 

// /* It should work */
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_MARGHERITA);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra tomatoes
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.



// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient