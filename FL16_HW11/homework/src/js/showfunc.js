export let showLiked = function() {
    let mainPage = location.href;
    let tweetItems = document.getElementById('tweetItems');
    let mainHeader = tweetItems.firstElementChild;
    let mainH1Liked = 'Liked tweet';
    let list = document.getElementById('list');
    let liked = document.getElementById('liked');
    location.assign(mainPage + '#/liked');
    mainHeader.innerText = mainH1Liked;
    if (liked.innerText !== "Back") {
        liked.innerText = 'Back';
    } else {
        liked.innerText = 'View liked';
    }

    liked.removeEventListener('click', showLiked);
    liked.addEventListener('click', () => {
        history.back();
    })

    for (let i = 0; i < list.children.length; i++) {
        list.children[i].classList.toggle('hidden');
        for (let key in localStorage) {
            if (key === list.children[i].dataset.number) {
                list.children[key].classList.toggle('hidden');
                list.children[key].lastElementChild.previousElementSibling.classList.toggle('hidden');
            }
        }
    }
}