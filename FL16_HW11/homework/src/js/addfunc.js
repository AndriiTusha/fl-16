    import { likeTweet } from './likefunc.js';

    export let addTweetFunc = function(e) {
        e.preventDefault();
        let modifyItem = document.getElementById('modifyItem');
        let modifyItemHeader = document.getElementById('modifyItemHeader');
        let modifyItemInput = document.getElementById('modifyItemInput');
        let list = document.getElementById('list');
        let tweetItems = document.getElementById('tweetItems');
        let saveModifiedItem = document.getElementById('saveModifiedItem');
        let alertMessage = document.getElementById('alertMessage');
        let alertMessageText = document.getElementById('alertMessageText');

        function getSpanNumber(e) {
            let editTweetFunc = function() {
                let modifyItemInput = document.getElementById('modifyItemInput');
                let spanNumber = localStorage.getItem('spanNumber');
                let targetSpanArr = list.children;
                for (let i = 0; i < targetSpanArr.length; i++) {
                    if (targetSpanArr[i].dataset.number === spanNumber) {
                        let inputText = modifyItemInput.value;
                        targetSpanArr[i].firstElementChild.innerText = inputText;
                        tweetItems.classList.toggle('hidden');
                        modifyItem.classList.toggle('hidden');
                        modifyItemHeader.innerText = modifyItemH1Edit;
                        history.back();
                    }
                }
            };
            let mainPage = location.href;
            //let modifyItemH1Add = 'Add tweet';
            let modifyItemH1Edit = 'Edit tweet';
            //let mainH1Liked = 'Liked tweet';
            let spanNumber = e.target.parentElement.dataset.number;
            localStorage.setItem('spanNumber', spanNumber);
            if (location.hash) {
                location.hash = '';
            }
            location.assign(mainPage + '#/edit/:' + spanNumber);
            modifyItemInput.value = '';
            tweetItems.classList.toggle('hidden');
            modifyItem.classList.toggle('hidden');
            modifyItemHeader.innerText = modifyItemH1Edit;
            saveModifiedItem.removeEventListener('click', addTweetFunc);
            saveModifiedItem.addEventListener('click', editTweetFunc);
        }

        let removeTweet = function(e) {
            if (e.target.getAttribute('id') !== 'remove') {
                return;
            }
            e.target.parentElement.remove();
        }

        let dataNumber = 0;
        let inputText = modifyItemInput.value;
        let prevTweetText = '';
        if (list.lastElementChild) {
            prevTweetText = list.lastElementChild.firstElementChild.innerText;
        }
        if (list.lastElementChild && prevTweetText === inputText) {
            alertMessageText.innerHTML = 'Error! You can`t tweet about that';
            alertMessage.classList.toggle('hidden');
            return;
        }
        if (inputText.length > 0 && inputText.length < 140) {
            if (alertMessage.className !== 'hidden') {
                alertMessage.classList.toggle('hidden');
            }
            tweetItems.classList.toggle('hidden');
            modifyItem.classList.toggle('hidden');
            let li = document.createElement('li');
            let buttonsHTML = '<button id="remove">Remove</button><button id="like">Like</button>';
            li.setAttribute('data-number', dataNumber++);
            li.insertAdjacentHTML('beforeend', `<span id="span-${dataNumber - 1}">${inputText}</span>${buttonsHTML}`);
            list.appendChild(li);
            document.getElementById(`span-${dataNumber - 1}`).addEventListener('click', getSpanNumber);
            tweetItems.addEventListener('click', removeTweet);
            tweetItems.addEventListener('click', likeTweet);
            inputText = '';
            history.back();
        }
    };