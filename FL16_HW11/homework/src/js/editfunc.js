export let editTweetFunc = function() {
    let spanNumber;
    let list = document.getElementById('list');
    let inputText = '';
    let modifyItemInput = document.getElementById('modifyItemInput');
    let tweetItems = document.getElementById('tweetItems');
    let modifyItem = document.getElementById('modifyItem');
    let modifyItemHeader = document.getElementById('modifyItemHeader');
    let modifyItemH1Edit = 'Edit tweet';

    spanNumber = localStorage.getItem('spanNumber');
    let targetSpanArr = list.children;
    for (let i = 0; i < targetSpanArr.length; i++) {
        if (targetSpanArr[i].dataset.number === spanNumber) {
            inputText = modifyItemInput.value;
            targetSpanArr[i].firstElementChild.innerText = inputText;
            tweetItems.classList.toggle('hidden');
            modifyItem.classList.toggle('hidden');
            modifyItemHeader.innerText = modifyItemH1Edit;
            history.back();
        }
    }
};