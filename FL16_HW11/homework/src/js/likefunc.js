import { showLiked } from "./showfunc";
export let likeTweet = function(e) {
    e.preventDefault();
    if (e.target.getAttribute('id') === 'like') {
        let alertMessage = document.getElementById('alertMessage');
        let alertMessageText = document.getElementById('alertMessageText');
        let tagNameStr = e.target.parentElement.firstElementChild.innerText;
        let tagNumber = e.target.parentElement.dataset.number;
        let mass = [];
        if (e.target.innerText === 'Like') {
            localStorage.setItem(tagNumber, tagNameStr);
            e.target.innerText = 'Unlike';
            let likeCollects = Array.from(document.getElementById('list').children);
            mass = likeCollects.filter(item => item.lastElementChild.innerText === 'Like');
            if (mass.length >= 0 || mass.length < likeCollects.length - 1) {
                document.getElementById('liked').classList = 'visible';
                document.getElementById('liked').onclick = showLiked;
            }
            alertMessage.classList.toggle('hidden');
            alertMessageText.innerText = `Hooray! You liked tweet with id ${tagNumber}`;
            setTimeout(() => {
                alertMessage.classList.toggle('hidden')
            }, 2000);
        } else {
            let likeCollects = Array.from(document.getElementById('list').children);
            localStorage.removeItem(tagNumber);
            e.target.innerText = 'Like';
            mass = likeCollects.filter(item => item.lastElementChild.innerText === 'Unlike');
            if (mass.length === 0) {
                document.getElementById('liked').classList.toggle('hidden');
            }
            alertMessage.classList.toggle('hidden');
            alertMessageText.innerText = `Oyy! You unliked this tweet with id ${tagNumber}`;
            setTimeout(() => {
                alertMessage.classList.toggle('hidden')
            }, 2000);
        }
    }
}