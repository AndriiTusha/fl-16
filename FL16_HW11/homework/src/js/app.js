//import { likeTweet } from './js/likefunc.js';
import { addTweetFunc } from './js/addfunc.js';
//import { showLiked } from './js/showfunc.js';
//import { editTweetFunc } from './js/editfunc.js';
import './scss/style.scss';

//const root = document.getElementById('root');
// let alertMessage = document.getElementById('alertMessage');
// let alertMessageText = document.getElementById('alertMessageText');
let tweetItems = document.getElementById('tweetItems');
let navigationButtons = document.getElementById('navigationButtons');
let addTweet = navigationButtons.firstElementChild;
// let list = document.getElementById('list');
// let mainHeader = tweetItems.firstElementChild;
let modifyItem = document.getElementById('modifyItem');
let modifyItemHeader = document.getElementById('modifyItemHeader');
// let modifyItemInput = document.getElementById('modifyItemInput');
let cancelModification = document.getElementById('cancelModification');
let saveModifiedItem = document.getElementById('saveModifiedItem');
//let inputText = '';
let modifyItemH1Add = 'Add tweet';
// let modifyItemH1Edit = 'Edit tweet';
// let mainH1Liked = 'Liked tweet';
let mainPage = location.href;
//let dataNumber = 0;
//let spanNumber;
// let likeCollects = [];
// let tagNameStr;
// let tagNumber;
// let mass;
// let prevTweetText;
// let li;
// let buttonsHTML;

console.log(localStorage);

let showLikedButtonHTML = '<button id="liked" class="liked hidden">View Liked</button>';
localStorage.clear();
navigationButtons.insertAdjacentHTML('beforeend', showLikedButtonHTML);

// let removeTweet = function(e) {
//     if (e.target.getAttribute('id') !== 'remove') {
//         return;
//     }
//     e.target.parentElement.remove();
// }

// let showLiked = function() {
//     location.assign(mainPage + '#/liked');
//     mainHeader.innerText = mainH1Liked;
//     if (liked.innerText !== "Back") {
//         liked.innerText = 'Back';
//     } else {
//         liked.innerText = 'View liked';
//     }

//     liked.removeEventListener('click', showLiked);
//     liked.addEventListener('click', () => {
//         history.back();
//     })

//     for (let i = 0; i < list.children.length; i++) {
//         list.children[i].classList.toggle('hidden');
//         for (key in localStorage) {
//             if (key === list.children[i].dataset.number) {
//                 list.children[key].classList.toggle('hidden');
//                 list.children[key].lastElementChild.previousElementSibling.classList.toggle('hidden');
//             }
//         }
//     }
// }

//import { likeTweet } from './likefunc';

// let likeTweet = function(e) {
//     e.preventDefault();
//     if (e.target.getAttribute('id') === 'like') {
//         let tagNameStr = e.target.parentElement.firstElementChild.innerText;
//         let tagNumber = e.target.parentElement.dataset.number;
//         let mass = [];
//         if (e.target.innerText === 'Like') {
//             localStorage.setItem(tagNumber, tagNameStr);
//             e.target.innerText = 'Unlike';
//             likeCollects = Array.from(document.getElementById('list').children);
//             mass = likeCollects.filter(item => item.lastElementChild.innerText === 'Like');
//             if (mass.length >= 0 || mass.length < likeCollects.length - 1) {
//                 document.getElementById('liked').classList = 'visible';
//                 document.getElementById('liked').onclick = showLiked;
//             }
//             alertMessage.classList.toggle('hidden');
//             alertMessageText.innerText = `Hooray! You liked tweet with id ${tagNumber}`;
//             setTimeout(() => {
//                 alertMessage.classList.toggle('hidden')
//             }, 2000);
//         } else {
//             localStorage.removeItem(tagNumber);
//             e.target.innerText = 'Like';
//             mass = likeCollects.filter(item => item.lastElementChild.innerText === 'Unlike');
//             if (mass.length === 0) {
//                 document.getElementById('liked').classList.toggle('hidden');
//             }
//             alertMessage.classList.toggle('hidden');
//             alertMessageText.innerText = `Oyy! You unliked this tweet with id ${tagNumber}`;
//             setTimeout(() => {
//                 alertMessage.classList.toggle('hidden')
//             }, 2000);
//         }
//     }
// }

addTweet.addEventListener('click', (e) => {
    e.preventDefault();
    location.assign(mainPage + '#/add');
    modifyItemHeader.innerText = '';
    modifyItemHeader.innerText = modifyItemH1Add;
    tweetItems.classList.toggle('hidden');
    modifyItem.classList.toggle('hidden');
    saveModifiedItem.addEventListener('click', addTweetFunc);
})


//import { addTweetFunc } from './addfunc';

// let addTweetFunc = function(e) {
//     e.preventDefault();
//     inputText = modifyItemInput.value;
//     let prevTweetText = '';
//     if (list.lastElementChild) {
//         prevTweetText = list.lastElementChild.firstElementChild.innerText;
//     }
//     if (list.lastElementChild && prevTweetText === inputText) {
//         alertMessageText.innerHTML = 'Error! You can`t tweet about that';
//         alertMessage.classList.toggle('hidden');
//         return;
//     }
//     if (inputText.length > 0 && inputText.length < 140) {
//         if (alertMessage.className !== 'hidden') {
//             alertMessage.classList.toggle('hidden');
//         }
//         tweetItems.classList.toggle('hidden');
//         modifyItem.classList.toggle('hidden');
//         let li = document.createElement('li');
//         let buttonsHTML = '<button id="remove">Remove</button><button id="like">Like</button>';
//         li.setAttribute('data-number', dataNumber++);
//         li.insertAdjacentHTML('beforeend', `<span id="span-${dataNumber - 1}">${inputText}</span>${buttonsHTML}`);
//         list.appendChild(li);
//         document.getElementById(`span-${dataNumber - 1}`).addEventListener('click', getSpanNumber);
//         tweetItems.addEventListener('click', removeTweet);
//         tweetItems.addEventListener('click', likeTweet);
//         inputText = '';
//         history.back();
//     }
// };

saveModifiedItem.addEventListener('click', addTweetFunc);

cancelModification.addEventListener('click', (e) => {
    e.preventDefault();
    history.back();
    tweetItems.classList.toggle('hidden');
    modifyItem.classList.toggle('hidden');
})

// let editTweetFunc = function() {
//     spanNumber = localStorage.getItem('spanNumber');
//     let targetSpanArr = list.children;
//     for (let i = 0; i < targetSpanArr.length; i++) {
//         if (targetSpanArr[i].dataset.number === spanNumber) {
//             inputText = modifyItemInput.value;
//             targetSpanArr[i].firstElementChild.innerText = inputText;
//             tweetItems.classList.toggle('hidden');
//             modifyItem.classList.toggle('hidden');
//             modifyItemHeader.innerText = modifyItemH1Edit;
//             history.back();
//         }
//     }
// };


// function getSpanNumber(e) {
//     spanNumber = e.target.parentElement.dataset.number;
//     localStorage.setItem('spanNumber', spanNumber);
//     if (location.hash) {
//         location.hash = '';
//     }
//     location.assign(mainPage + '#/edit/:' + spanNumber);
//     modifyItemInput.value = '';
//     tweetItems.classList.toggle('hidden');
//     modifyItem.classList.toggle('hidden');
//     modifyItemHeader.innerText = modifyItemH1Edit;
//     saveModifiedItem.removeEventListener('click', addTweetFunc);
//     saveModifiedItem.addEventListener('click', editTweetFunc);
// }