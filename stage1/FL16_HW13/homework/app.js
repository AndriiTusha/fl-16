const appRoot = document.getElementById('app-root');

let headerSnippet0 = '<header id="header"><form><h1>Countries Search</h1><div class="searchTop"><p>Please choose ';
let headerSnippet1 = 'type of search</p><div class="radio"><label for="region"><input type="radio" name="radioSelect"';
let headerSnippet2 = ' value="region" id="region">By Region</label> <label for="language">';
let headerSnippet3 = '<input type="radio" name="radioSelect" value="language" id="language">By Language</label></div>';
let headerSnippet4 = '</div><div class="queryBy"> <label for="queryBy">Please choose search query: <select name=';
let headerSnippet5 = '"queryBy" id="queryBySelect" disabled><option value="disabled" id="disabled">Select value';
let headerSnippet6 = headerSnippet4 + headerSnippet5 + '</option></select></label></div></form></header>';

let headerSnippet = headerSnippet0 + headerSnippet1 + headerSnippet2 + headerSnippet3 + headerSnippet6;

appRoot.insertAdjacentHTML('beforeend', headerSnippet);

let queryBySelect = document.getElementById('queryBySelect');
let optionDisabled = document.getElementById('disabled');
let radioSelect = document.querySelectorAll('input[name=radioSelect]');
let header = document.getElementById('header');

// get list from external sources
let optionList;

function getList(listName) {
    optionList = '';

    for (let key in listName) {
        if (Object.hasOwnProperty.call(listName, key)) {
            optionList = optionList + '<option value="' + listName[key] + '">' + listName[key] + '</option>';
        }
    }
    return optionList;
}

for (let i = 0; i < radioSelect.length; i++) {
    radioSelect[i].addEventListener('click', () => {
        if (radioSelect[i].type === 'radio' && radioSelect[i].checked) {
            queryBySelect.disabled = false;
            switch (radioSelect[i].value) {
                case 'region':
                    getList(externalService.getRegionsList());
                    if (queryBySelect.lastChild.value === optionDisabled.value) {
                        queryBySelect.insertAdjacentHTML('beforeend', optionList);
                    } else {
                        while (queryBySelect.lastChild.value !== optionDisabled.value) {
                            queryBySelect.lastChild.remove();
                        }
                        queryBySelect.insertAdjacentHTML('beforeend', optionList);
                    }
                    break;
                case 'language':
                    getList(externalService.getLanguagesList());
                    if (queryBySelect.lastChild.value === optionDisabled.value) {
                        queryBySelect.insertAdjacentHTML('beforeend', optionList);
                    } else {
                        while (queryBySelect.lastChild.value !== optionDisabled.value) {
                            queryBySelect.lastChild.remove();
                        }
                        queryBySelect.insertAdjacentHTML('beforeend', optionList);
                    }
                    break;
                default:
                    alert('Error here!');
            }

            let noItemMessage = '<p class = "no-item-text">No items, please choose search query</p>';
            if (header.lastChild.className !== 'no-item-text') {
                header.insertAdjacentHTML('beforeend', noItemMessage);
            }
        }
    });
}

queryBySelect.onchange = (e) => {
    if (e.target.value && radioSelect[0].checked) {
        let countryRegion = externalService.getCountryListByRegion(e.target.value);
        createTable(countryRegion);
    }
    if (e.target.value && radioSelect[1].checked) {
        let countryLanguage = externalService.getCountryListByLanguage(e.target.value);
        createTable(countryLanguage);
    }
};

function createTable(value) {
    if (header.lastChild.className === 'no-item-text') {
        header.lastChild.remove();
    }

    if (header.nextElementSibling !== null) {
        header.nextElementSibling.remove();
    }
    let snippetThead1 = '<thead id="thead"><th id="countryName">Contry Name <span id = "countryNameArrow"></span></th>';
    let snippetThead2 = '<th>Capital</th><th>World Region</th><th>Languages</th><th id="areaName">Area <span ';
    let snippetThead3 = 'id="areaNameArrow">&#8691;</span></th><th>Flag</th></thead><tbody id="tbody"></tbody>';
    let snippetThead0 = '<section class="section"><table id="table">';
    let tableHTMLMain = snippetThead0 + snippetThead1 + snippetThead2 + snippetThead3 + '</table></section>';
    appRoot.insertAdjacentHTML('beforeend', tableHTMLMain);
    let tbody = document.getElementById('tbody');
    let countryName = document.getElementById('countryName');
    let countryNameArrow = document.getElementById('countryNameArrow');
    let areaName = document.getElementById('areaName');
    let areaNameArrow = document.getElementById('areaNameArrow');
    value.sort((a, b) => a['name'] > b['name'] ? 1 : -1);
    tbodyCreate(value);
    countryName.className = 'ASC';
    areaName.className = 'ASC';
    countryName.onclick = () => {
        if (countryName.className === 'ASC') {
            while (tbody.firstChild) {
                tbody.removeChild(tbody.firstChild);
            }
            countryNameArrow.innerHTML = '';
            value.sort((a, b) => a['name'] < b['name'] ? 1 : -1);
            tbodyCreate(value);
            countryName.className = 'DCS';
            countryNameArrow.innerHTML = '&#8681;';
        } else {
            while (tbody.firstChild) {
                tbody.removeChild(tbody.firstChild);
            };
            countryNameArrow.innerHTML = '';
            value.sort((a, b) => a['name'] > b['name'] ? 1 : -1);
            tbodyCreate(value);
            countryName.className = 'ASC';
            countryNameArrow.innerHTML = '&#8679;';
        }
    };

    areaName.onclick = () => {
        if (areaName.className === 'ASC') {
            while (tbody.firstChild) {
                tbody.removeChild(tbody.firstChild);
            }
            areaNameArrow.innerHTML = '';
            value.sort((a, b) => a['area'] < b['area'] ? 1 : -1);
            tbodyCreate(value);
            areaName.className = 'DCS';
            areaNameArrow.innerHTML = '&#8681;';
        } else {
            while (tbody.firstChild) {
                tbody.removeChild(tbody.firstChild);
            };
            areaNameArrow.innerHTML = '';
            value.sort((a, b) => a['area'] > b['area'] ? 1 : -1);
            tbodyCreate(value);
            areaName.className = 'ASC';
            areaNameArrow.innerHTML = '&#8679;';
        }
    };

}

function getLanguage(langObj) {
    if (langObj !== null) {
        let langList = '';
        for (let key in langObj) {
            langList += `${langObj[key]} `;
        }
        return langList;
    } else {
        return langObj;
    }
}

function tbodyCreate(value) {
    let tbodyContent = '';
    for (let i = 0; i < value.length; i++) {
        let valueName = `<td>${value[i]['name']}</td>`;
        let capitalRegionName = `<td>${value[i]['capital']}</td><td>${value[i]['region']}</td>`;
        let langAreaName = `<td>${getLanguage(value[i]['languages'])}</td><td>${value[i]['area']}</td>`;
        let imgName = `<td style="background-image:url(${value[i]['flagURL']});"></td>`;
        tbodyContent += `<tr>` + valueName + capitalRegionName + langAreaName + imgName + `</tr>`;
    }
    tbody.insertAdjacentHTML('beforeend', tbodyContent);
}


/*
write your code here

list of all regions
externalService.getRegionsList();
list of all languages
externalService.getLanguagesList();

get countries list by language
externalService.getCountryListByLanguage()

get countries list by region
externalService.getCountryListByRegion()
*/