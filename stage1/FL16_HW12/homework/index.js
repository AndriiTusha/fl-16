const birthday22 = new Date(2000, 9, 22);
const birthday23 = new Date(2000, 9, 23);
getAge(birthday22); // 20 (assuming today is the 22nd October)
getAge(birthday23); // 19 (assuming today is the 22nd October)

function getAge(date) {
    let now = new Date(2020, 9, 22);
    let dayNow = now.getDate();
    let monthNow = now.getMonth();
    let yearNow = now.getFullYear();
    let today = new Date(yearNow, monthNow, dayNow);
    let birthdayDay = date.getDate();
    let birthdayMonth = date.getMonth();
    let birthdayYear = date.getFullYear();
    let birthdayinThisYear = new Date(yearNow, birthdayMonth, birthdayDay);
    let age;
    age = yearNow - birthdayYear;
    if (today < birthdayinThisYear) {
        age = age - 1;
    }
    if (age < 0) {
        console.log(`Please check input date`);
        return;
    }
    return age;
}

getWeekDay(Date.now()); // "Thursday" (if today is the 22nd October)
getWeekDay(new Date(2020, 9, 22)); // "Thursday"

function getWeekDay(value) {
    let weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let day = new Date(+value).getDay();
    let dayName = '';
    for (let key in weekdays) {
        if (day === +key) {
            dayName = weekdays[key];
        }
    }
    return dayName;
}

getAmountDaysToNewYear('2021-08-30'); // 124 (if today is the 30th August)
getAmountDaysToNewYear('2021-01-01'); // 365 (if today is the 1st January)

function getAmountDaysToNewYear(value) {
    let now = new Date(value);
    now.setHours(0, 0, 0);
    let nowYear = now.getFullYear();
    let newYearnow = new Date(nowYear + 1, 0, 1);
    let diff = newYearnow - now;
    let roundedDiff = (Math.round(diff / 864000) / 100).toFixed(0);
    return roundedDiff;
}

getProgrammersDay(2020); // "12 Sep, 2020 (Saturday)"
getProgrammersDay(2019); // "13 Sep, 2019 (Friday)"

function getProgrammersDay(year) {
    let weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let startOfTheYear = new Date(year, 0, 1);
    startOfTheYear.setHours(0, 0, 0);
    let diff = 255 * 24 * 3600 * 1000;
    let progDayInMs = +startOfTheYear + diff;
    let progDay = new Date(progDayInMs);
    progDay.setHours(0, 0, 0);
    let progDayWeekDayNo = progDay.getDay();
    let progDayMonthNo = progDay.getMonth();
    let dayName = '';
    let monthName = '';
    for (let key in weekdays) {
        if (progDayWeekDayNo === +key) {
            dayName = weekdays[key];
        }
    }
    for (let key in monthNames) {
        if (progDayMonthNo === +key) {
            monthName = monthNames[key];
        }
    }
    let str = `${progDay.getDate()} ${monthName}, ${year} (${dayName}) `;
    return str;
}

howFarIs('friday'); // "It's 1 day(s) left till Friday." (on October 22nd)
howFarIs('Thursday'); // "Hey, today is Thursday =)" (on October 22nd)

function howFarIs(str) {
    let weekdays = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
    let outputStr = '';
    let now = new Date();
    let dayNoNow = now.getDay();
    let dayNoChoosed = 0;
    for (let key in weekdays) {
        if (str.toLowerCase() === weekdays[key]) {
            dayNoChoosed = +key;
        }
    }
    let outputChoosedDayName = weekdays[dayNoChoosed][0].toUpperCase() + weekdays[dayNoChoosed].slice(1);
    if (dayNoChoosed === dayNoNow) {
        outputStr = `"Hey, today is ${outputChoosedDayName} =)"`;
    } else if (dayNoChoosed > dayNoNow) {
        outputStr = `It's ${dayNoChoosed - dayNoNow} day(s) left till ${outputChoosedDayName}.`;
    } else {
        outputStr = `It's ${7 - (dayNoNow - dayNoChoosed)} day(s) left till ${outputChoosedDayName}.`;
    }
    return outputStr;
}

isValidIdentifier('myVar!'); // false
isValidIdentifier('myVar$'); // true
isValidIdentifier('myVar_1'); // true
isValidIdentifier('1_myVar'); // false

function isValidIdentifier(str) {
    let patternStr = /^[^0-9~!@#%^&*()\-+='"/|`<>., ][A-Za-z0-9_$]*$/gmi;
    return patternStr.test(str);
}

const testStr = 'My name is John Smith. I am 27.';
capitalize(testStr); // "My Name Is John Smith. I Am 27."

function capitalize(str) {
    let patternStr = /^[a-z]{1}/gi;
    let wordArr = [];
    wordArr = str.split(' ');
    for (let i = 0; i < wordArr.length; i++) {
        if (patternStr.test(wordArr[i])) {
            wordArr[i] = wordArr[i][0].toUpperCase() + wordArr[i].slice(1);
        }
    }
    return wordArr.join(' ');
}

isValidAudioFile('file.mp4'); // false
isValidAudioFile('my_file.mp3'); // false
isValidAudioFile('file.mp3'); // true

function isValidAudioFile(str) {
    let patternStr = /^([A-Za-z]+)\.(mp3|flac|alac|aac)$/g;
    return patternStr.test(str);
}

const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';
getHexadecimalColors(testString); // ["#3f3", "#AA00ef"]
getHexadecimalColors('red and #0000'); // [];

function getHexadecimalColors(str) {
    let patternStr = /#[abcdef0-9]{3}\b|#[abcdef0-9]{6}\b/gi;
    let arr = [];
    arr = str.match(patternStr);
    return arr;
}


isValidPassword('agent007'); // false (no uppercase letter)
isValidPassword('AGENT007'); // false (no lowercase letter)
isValidPassword('AgentOOO'); // false (no numbers)
isValidPassword('Age_007'); // false (too short)
isValidPassword('Agent007'); // true

function isValidPassword(str) {
    let patternStr = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*[\W]).{8,}/g;
    return patternStr.test(str);
}

addThousandsSeparators('1234567890'); // "1,234,567,890"
addThousandsSeparators(1234567890); // "1,234,567,890"

function addThousandsSeparators(str) {
    let strType = typeof str;
    if (strType !== 'string') {
        str = str.toString();
    }
    let patternStr = /\d{1,3}(?=(\d{3})*$)/g;
    let arr = [];
    arr = str.match(patternStr);
    let newStr = arr.join(',');
    return newStr;
}


const text1 = 'We use https://translate.google.com/ to translate some words and phrases from https://angular.io/ ';
const text2 = 'JavaScript is the best language for beginners!'
getAllUrlsFromText(text1); // ["https://translate.google.com/", "https://angular.io/"]
getAllUrlsFromText(text2); // []
getAllUrlsFromText(); // (error)

function getAllUrlsFromText(str) {
    let patternStr = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-/]))?/gi;
    let arr = [];
    //console.log(str);
    try {
        if (str === undefined) {
            throw new Error('Error');
        } else {
            arr = str.match(patternStr);
        }
    } catch (e) {
        return e.message;
    }
    if (arr === null) {
        arr = [];
        return arr;
    } else {
        return arr;
    }
}