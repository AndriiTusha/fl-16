/* START TASK 1: Your code goes here */

let selectedTd;
let table = document.getElementById('tableTask1');
let trArr = document.getElementsByTagName('tr');

table.onclick = function(e) {
    let target = e.target;
    if (target.tagName !== 'TD') {
        return;
    }
    highlight(target);
};

function highlight(td) {
    if (td.style.background === 'white' || td.style.background === '') {
        td.style.background = 'yellow';
    } else {
        td.style.background = 'white';
    }
    for (let i = 0; i < trArr.length; i++) {
        if (trArr[i].firstElementChild === td) {
            let setOfTd = trArr[i].children;
            setOfTd[0].style.background = 'green';
            for (let j = 0; j < setOfTd.length; j++) {
                if (setOfTd[j].style.background !== 'yellow') {
                    setOfTd[j].style.background = 'green';
                }
            }
        }
    }
    if (td.getAttribute('id') === 'special') {
        let specialTd = td;
        specialTd.style.background = 'blue';
        for (let i = 0; i < trArr.length; i++) {
            for (let j = 0; j < trArr[i].children.length; j++) {
                if (trArr[i].children[j].style.background === '' || trArr[i].children[j].style.background === 'white') {
                    trArr[i].children[j].style.background = 'blue';
                }
            }
        }
    }
}

/* END TASK 1 */

/* START TASK 2: Your code goes here */

let input = document.getElementById('input');
let button = document.getElementById('button');
let errorDiv = document.getElementById('errorDiv');
let successDiv = document.getElementById('successDiv');
let inputValue = '';
const numberRegExp = /^\+380\d{9}$/;
input.onblur = () => {
    inputValue = input.value;
    if (numberRegExp.test(inputValue)) {
        if (errorDiv.style.display === 'block') {
            errorDiv.style.display = 'none';
        }
        input.style.border = "none";
        input.style.border = "2px solid green";
        button.disabled = false;
    } else {
        errorDiv.style.display = 'block';
        input.style.border = "none";
        input.style.border = "2px solid red";
        button.disabled = true;
    }
}

button.onclick = (e) => {
    e.preventDefault();
    successDiv.style.display = 'block';
}

/* END TASK 2 */

/* START TASK 3: Your code goes here */

let court = document.getElementById('court');
let ball = document.getElementById('ball');
let teamA = document.getElementById('teamA');
let teamB = document.getElementById('teamB');
let message = document.getElementById('message');
let scoreDiv = document.getElementById('scoreDiv');
let countA = 0;
let countB = 0;
let count = 0;
let color = '';
let teamName = '';
let teamNameA = 'A';
let teamNameB = 'B';
let redColor = 'red';
let blueColor = 'blue';

function showDiv() {
    scoreDiv.style.border = `2px solid ${color}`;
    teamA.innerHTML = `Team ${teamNameA}: ${countA}`;
    teamB.innerHTML = `Team ${teamNameB}: ${countB}`;
    message.innerHTML = `Congratulation! Team ${teamName} Score!`;
    message.style.color = `${color}`;
    scoreDiv.classList.toggle('hidden');
    scoreDiv.style.transition = "opacity 3s";
    setTimeout(() => {
        if (!scoreDiv.classList[1]) {
            scoreDiv.classList.toggle('hidden');
        }
    }, 3000);
}
let customEvent = new Event("congrats", { bubbles: true });
document.addEventListener('congrats', showDiv);

court.addEventListener('click', (e) => {
    let courtClickX = e.offsetX;
    let courtClickY = e.offsetY;
    ball.style.left = courtClickX - 20 + 'px';
    ball.style.top = courtClickY - 20 + 'px';

    let pointOneX = 40;
    let pointOneY = 165;
    let pointTwoX = 560;
    let pointTwoY = 165;

    let ballXAfterRemove = parseInt(ball.style.left);
    let ballYAfterRemove = parseInt(ball.style.top);

    console.log(ballXAfterRemove, ballYAfterRemove);

    let coordX = ballXAfterRemove >= pointOneX - 15 && ballXAfterRemove <= pointOneX + 15;
    let coordY = ballYAfterRemove >= pointOneY - 15 && ballYAfterRemove <= pointOneY + 15;

    if (coordX && coordY) {
        countB++;
        teamName = teamNameB;
        count = countB;
        color = redColor;
        scoreDiv.dispatchEvent(customEvent);
    }
    let coordXX = ballXAfterRemove >= pointTwoX - 15 && ballXAfterRemove <= pointTwoX + 15;
    let coordYY = ballYAfterRemove >= pointTwoY - 15 && ballYAfterRemove <= pointTwoY + 15;
    if (coordXX && coordYY) {
        countA++;
        teamName = teamNameA;
        count = countA;
        color = blueColor;
        scoreDiv.dispatchEvent(customEvent);
    }
})

/* END TASK 3 */