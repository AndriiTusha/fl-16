let agree = confirm('Do you want to play game?');
let attempts = 3;
let pocketPattern = /^[0-8]$/;
let maxPrize = 100;
let medPrize = maxPrize / 2;
let minPrize = medPrize / 2;
let zeroPrize = 0;
let totalPrize = 0;
let attemptPrize = 0;
let wishFlag;
let min = 0;
let max = 8;
let count = 0;
let pocket;
let pocketRandom;
let startScale = 0;
let scale = 4;

function randomInteger(min, max) {

    // случайное число от min до (max+1)
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

function game(multi) {
    pocket = +pocket; //Str to Number

    if (multi === 0) {
        maxPrize;
        max;
    } else {
        maxPrize = maxPrize * multi / 2;
        medPrize = maxPrize / 2;
        minPrize = medPrize / 2;
        max = max + multi;
    }

    for (let i = 0; i < attempts; i++) {

        switch (i) {
            case 0:
                attemptPrize = maxPrize;
                break;
            case 1:
                attemptPrize = medPrize;
                break;
            case 2:
                attemptPrize = minPrize;
                break;
            default:
                attemptPrize = zeroPrize;
        }

        alert(`Attempts:${attempts-i}\nTotal:${totalPrize}$\nAttempt prize:${attemptPrize}$\nMax:${max}`);
        pocketRandom = randomInteger(min, max); //generate a random number from min to max
        console.log(pocket, pocketRandom);
        if (pocket === pocketRandom && i === 0) {
            alert(`Congratulation, you won! Your prize is: ${maxPrize}$.`);
            totalPrize += maxPrize;
            break;
        }
        if (pocket === pocketRandom && i === 1) {
            alert(`Congratulation, you won! Your prize is: ${medPrize}$.`);
            totalPrize += medPrize;
            break;
        }
        if (pocket === pocketRandom && i === 2) {
            alert(`Congratulation, you won! Your prize is: ${minPrize}$.`);
            totalPrize += minPrize;
            break;
        } else {
            alert(`Thank you for your participation. Your prize is: ${zeroPrize}.`);
            totalPrize += zeroPrize;
        }
    } //end loop
    alert(`Total:${totalPrize}$`);
    wishFlag = confirm(`Do you want to continue?`);
    count++;
    if (wishFlag) {
        game(scale);
    } else {
        alert('It was nice game!');
    }
}
if (agree) {
    do {
        pocket = prompt('Choose a roulette pocket number from 0 to 8', max);

        if (pocket === null) {
            alert('You press cancel');
            break;
        } else if (!pocketPattern.test(pocket)) {
            alert('Invalid input data');
        } else {
            game(startScale);
        }
    }
    while (!pocketPattern.test(pocket));

} else {
    alert('You did not become a billionaire, but can.');
}