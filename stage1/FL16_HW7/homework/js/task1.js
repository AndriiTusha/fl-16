let amount = 0;
let initialAmount = 0;
let totalAmount = 0;
let amountPattern = /^([1-9]{1}\d{3,})(\.\d{0,2})?$/;
let term = 0;
let termPattern = /^([1-9]\d*)$/;
let rate = 0;
let maxRateLimit = 100;
let ratePattern = /^([1-9]{1}\d?[0]?)(\.\d{0,2})?$/;
let finalAmount = 0;
let totalProfit = 0;
let fixedLimit = 2;
let arrAmount = [];


do {
    amount = prompt('Enter your amount i.e. 1234.56: ', 1000);
    if (amount === null) {
        alert('You press cancel. Goodbye!');
        break;
    } else if (!amountPattern.test(amount)) {
        alert('Invalid input data');
    } else {
        amount = +amount;
        initialAmount = amount;
    }
} while (!amountPattern.test(amount));

do {
    term = prompt('Enter the term, years: (must be digits, term>=1, positive, integer)', 1);
    if (term === null) {
        alert('You press cancel. Goodbye!');
        break;
    } else if (!termPattern.test(term)) {
        alert('Invalid input data');
    } else {
        term = +term;
    }
} while (!termPattern.test(term));

do {
    rate = prompt('Enter the rate, %: (must be in digits, 1 =< rate <= 100, positive, only 2 digit after point)', 10);
    if (rate === null) {
        alert('You press cancel. Goodbye!');
        break;
    } else if (!ratePattern.test(rate) || rate > maxRateLimit) {
        alert('Invalid input data');
    } else {
        rate = +rate;
    }
} while (!ratePattern.test(rate) || rate > maxRateLimit);

for (let i = 0; i < term; i++) {
    amount += amount * (rate / 100);
    arrAmount[i] = Math.round(amount * 100) / 100;
}
totalAmount = Math.round(amount * 100) / 100;
totalProfit = Math.round((amount - initialAmount) * 100) / 100;
alert(`
       Initial amount: ${initialAmount} \n
       Number of years: ${term} \n
       Percentage of year: ${rate} \n
`);
for (let i = 0; i < term; i++) {
    alert(`${i + 1} Year \n 
                    Profit:${Math.round( (arrAmount[i] - initialAmount) * 100) / 100} (${rate}% from initial amount) \n 
                    Amount: ${arrAmount[i]} (initial amount + profit)
`);
}