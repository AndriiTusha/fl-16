function reverseNumber(num) {
    let flag = true;
    if (num < 0) {
        num *= -1;
        flag = false;
    };
    let startArr = num.toString().split('');
    let endArr = [];
    if (!flag) {
        endArr.push('-');
    }
    for (let i = startArr.length - 1; i >= 0; i--) {
        endArr.push(startArr[i]);
    };
    let finalNum = +endArr.join('');
    return finalNum;
}

function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}

function map(arr, func) {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
        newArr.push(func(arr[i]));
    }
    return newArr;
}

function filter(arr, func) {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
        if (func(arr[i])) {
            newArr.push(arr[i]);
        }
    }
    return newArr;
}

function getAdultAppleLovers(data) {
    let newArr = [];
    for (let i = 0; i < data.length; i++) {
        if (data[i]['age'] > 18 && data[i]['favoriteFruit'] === 'apple') {
            newArr.push(data[i]['name']);
        }
    }
    return newArr;
}

function getKeys(obj) {
    let newArr = [];
    for (let keys in obj) {
        newArr.push(keys);
    }
    return newArr;
}

function getValues(obj) {
    let newArr = [];
    for (let keys in obj) {
        newArr.push(obj[keys]);
    }
    return newArr;

}

function showFormattedDate(dateObj) {
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let date = dateObj.getDate();
    let month = dateObj.getMonth();
    let year = dateObj.getFullYear();
    for (let i = 0; i < months.length; i++) {
        if (i === month) {
            month = months[i];
        }
    }
    let finalStr = `It is ${date} of ${month}, ${year}`;
    return finalStr;
}