function isEquals(a, b) {
    return a === b;
}

function isBigger(a, b) {
    return a > b;
}

function storeNames(...str) {
    return str;
}

function getDifference(a, b) {
    if (a > b) {
        return a - b;
    } else {
        return b - a;
    }
}

function negativeCount(...arr) {
    let count = 0;
    let arrNew = [];
    arrNew = arr.join(',').split(',');
    for (let i = 0; i < arrNew.length; i++) {
        if (arrNew[i] < 0) {
            count++;
        }
    }
    return count;
}

function letterCount(str, substr) {
    let string = str.toLowerCase();
    let part = substr.toLowerCase();
    let result = [];
    let absent = -1;
    for (let i = 0; i < string.length; i++) {
        let partIndex = string.indexOf(part, i);
        if (partIndex !== absent) {
            result.push(partIndex);
            i = partIndex;
        }
    }
    return result.length;
}

function countPoints(...arr) {
    let arrNew = [];
    let teamArr = [];
    let count = 0;
    let win = 3;
    let draw = 1;
    let lose = 0;
    let chkFormatRegExp = /^([1-9]\d*):([1-9]\d*)$/;
    arrNew = arr.join(',').split(',');
    for (let i = 0; i < arrNew.length; i++) {
        if (chkFormatRegExp.test(arrNew[i])) {
            teamArr.push(arrNew[i].split(':'));
        } else {
            alert('Check input data');
            break;
        }
    }
    for (let j = 0; j < teamArr.length; j++) {
        if (+teamArr[j][0] > +teamArr[j][1]) {
            count += win;
        } else if (+teamArr[j][0] < +teamArr[j][1]) {
            count += lose;
        } else {
            count += draw;
        }
    }
    return count;
}