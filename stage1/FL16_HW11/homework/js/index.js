    function visitLink(path) {

        let links = document.querySelectorAll('a[href$="html"]');
        let count = window.localStorage.getItem(path);

        for (let i = 0; i < links.length; i++) {
            if (count === null) {
                count = 0;
            } else {
                count = window.localStorage.getItem(path);
            }

            if (links[i].innerHTML.replace(/\s+/g, '') === path) {
                window.localStorage.setItem(`${links[i].innerHTML.replace(/\s+/g, '')}`, ++count);
            }
        }
    }

    function viewResults() {
        let countPage1 = window.localStorage.getItem('Page1');
        let countPage2 = window.localStorage.getItem('Page2');
        let countPage3 = window.localStorage.getItem('Page3');

        if (countPage1 === null) {
            countPage1 = '0';
        } else if (countPage2 === null) {
            countPage2 = '0';
        } else {
            countPage3 = '0';
        }

        let div = document.getElementById('content');

        div.insertAdjacentHTML('beforeend', `<ul>
        <li>You visited Page1 ${countPage1} time(s)</li>
        <li>You visited Page2 ${countPage2} time(s)</li>
        <li>You visited Page3 ${countPage3} time(s)</li>
    </ul>`);
        window.localStorage.clear();
    }