let eventName = prompt('Enter name of event', 'meeting');
let toggle = document.getElementsByClassName('toggle')[0];
let confirmButton = document.getElementsByClassName('confirm-button')[0];
let converterButton = document.getElementsByClassName('converter-button')[0];
let nameFiled = document.getElementById('name');
let timeField = document.getElementById('time');
let placeField = document.getElementById('place');

let timeFieldPattern = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;

if (eventName) {
    toggle.classList.remove('toggle');
} else {
    alert('See you later!');
}

confirmButton.addEventListener('click', () => {

    if (nameFiled.value && placeField.value && timeFieldPattern.test(timeField.value)) {
        alert(`${nameFiled.value} has a ${eventName} today at ${timeField.value} somewere in ${placeField.value}`);
    } else if (nameFiled.value && placeField.value && !timeFieldPattern.test(timeField.value)) {
        alert('Enter time in format hh:mm');
    } else {
        alert('Input all data');
    }
});

converterButton.addEventListener('click', () => {
    let euro = prompt('Enter amount in EURO', 1);
    let usd = prompt('Enter amount in USD', 1);
    let rateEuroUAH = 32.85;
    let rateUSDUAH = 27.45;
    let totalEuroUAH = 0;
    let totalUSDUAH = 0;
    let chkNumberPattern = /^([1-9]\d*)(\.?[0-9]{0,2})$/;
    if (chkNumberPattern.test(euro) && chkNumberPattern.test(usd)) {
        +euro; +
        usd;
        totalEuroUAH = Math.round(rateEuroUAH * euro * 100) / 100;
        totalUSDUAH = Math.round(rateUSDUAH * usd * 100) / 100;
        alert(`${euro} euros = ${totalEuroUAH.toFixed(2)} hrns,\n${usd} dollars = ${totalUSDUAH.toFixed(2)} hrns`);

    } else {
        alert('Please check input data');
    }
});