// task-1

const arr = ['1', '3', '4', '2', '5'];

function getMaxEvenElement(array) {
    let numberArr = [];
    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] !== 'string') {
            console.log('There is not all string keys in array! Please check it');
            return;
        } else if (+array[i] % 2 === 0) {
            numberArr.push(+array[i]);
        }
    }
    let max = Math.max.apply(null, numberArr);
    return max;
}

console.log(getMaxEvenElement(arr));

// task-2

let a = 3;
let b = 5;

[b, a] = [a, b];

console.log(a); //5
console.log(b); //3

// task-3

const getValue = (value) => value ?? '-';

console.log(getValue(0)); //0
console.log(getValue(4)); //4
console.log(getValue('someText')); // someText
console.log(getValue(null)); // -
console.log(getValue(undefined)); // -

// task-4

const arrayOfArrays = [
    ['name', 'dan'],
    ['age', '21'],
    ['city', 'lviv']
];

const getObjFromArray = arrayOfArrays => Object.fromEntries(arrayOfArrays);

console.log(getObjFromArray(arrayOfArrays));

// task-5 

const obj1 = { name: 'nick' };

const addUniqueId = obj => {
    let id = Symbol();
    obj = {
        name: obj.name,
        id: id
    };
    return obj;
}

console.log(addUniqueId(obj1));
console.log(addUniqueId({ name: 'buffy' }));
console.log(Object.keys(obj1).includes('id'));

// task-6

const oldObj = {
    name: 'willow',
    details: { id: 1, age: 47, university: 'LNU' }
};

const getRegroupedObject = obj => {
    let newObj = {};
    newObj.university = obj.details.university;
    newObj.user = {};
    newObj.user.age = obj.details.age;
    newObj.user.firstName = obj.name;
    newObj.user.id = obj.details.id;

    return newObj;
}

console.log(getRegroupedObject(oldObj));

// task - 7

const arr7 = [2, 3, 4, 2, 4, 'a', 'c', 'a'];
const getArrayWithUniqueElements = arr7 => Array.from(new Set(arr7));
console.log(getArrayWithUniqueElements(arr7)); // [2,3,4,'a','c']

// task - 8

const phoneNumber = '0123456789';
const hideNumber = phoneNumber => phoneNumber.substr(phoneNumber.length - 4).padStart(phoneNumber.length, '*');
console.log(hideNumber(phoneNumber)); //******6789 /

// task - 9

const add = (a, b) => {
    return b ? a + b : (() => {
	throw new Error('b required');
	})();
}

console.log(add(2, 3)); //b
//console.log(add(2)); // error 'b required'

// task - 10

function* genetateIterableSequence() {
    yield 'I';
    yield 'love';
    yield 'EPAM';
}

const generatorObject = genetateIterableSequence();
for (let value of generatorObject) {
    console.log(value);
}

// I
// love
//EPAM