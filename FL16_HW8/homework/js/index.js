// Write your code here
//let $;
let part1 = '';
let part2 = '';
let sigh = '';
let strArr = [];
let regExp = /^([1-9]+[0]*)+([+\-/*]{1})?([1-9]+?[\d]*)?$/;
let result = 0;
let buttonValue = '';
let strPattern = '48';

function addResultField(a, b, operator, res) {
    $('#result-container').prepend(`<div id="result-line">
                <div class="circle"></div>
                <div id="resultField">${a} ${operator} ${b} = ${res}</div>
                <svg id="cross" width="30" height="30">
                  <line
                    x1="0" y1="0" x2="30" y2="30"
                    stroke="black" stroke-width="5"
                  />
                  <line
                    x1="0" y1="30" x2="30" y2="0"
                    stroke="black" stroke-width="5"
                  />
                </svg>
            </div>`);
    $('#result-line').on('click', (e) => {
        if (e.target.classList[0] === 'circle') {
            e.target.classList.toggle('circle-clicked');
        }
    });
    $('#cross').on('click', () => {
        $('#result-line').remove();
    });
    console.log(a, b, res.toString());
    if (a.includes(strPattern) || b.includes(strPattern) || res.toString().includes(strPattern)) {
        $('#result-line').css('text-decoration', 'underline');
    }
}

$('#buttons').find('td[id]').on('click', (e) => {
    buttonValue = e.target.innerText;

    if (strArr.length <= 15) {
        if (buttonValue !== '=' && buttonValue !== 'C') {
            strArr.push(buttonValue);
            if (strArr[0] === '0') {
                strArr = [];
            }
            let signIndex = strArr[strArr.length - 2];
            let zeroIndex = strArr[strArr.length - 1];

            if (signIndex === '/' || signIndex === '+' || signIndex === '*' || signIndex === '-') {
                if (zeroIndex === '0') {
                    $('#screen').val('ERROR');
                    strArr = [];
                }
            }
        }
        if (strArr.join('').match(regExp) !== null) {
            part1 = strArr.join('').match(regExp)[1];
            part2 = strArr.join('').match(regExp)[3];
            sigh = strArr.join('').match(regExp)[2];
            $('#screen').val(strArr.join(''));
        } else {
            strArr = [];
        }
    }
    if (buttonValue === 'C') {
        strArr = [];
        $('#screen').val('0');
    }
    if (buttonValue === '=') {
        switch (sigh) {
            case '/':
                result = Math.floor(+part1 / +part2) * 100 / 100;
                $('#screen').val(result);
                strArr = [];
                addResultField(part1, part2, sigh, result);
                break;

            case '*':
                result = +part1 * +part2;
                $('#screen').val(result);
                strArr = [];
                addResultField(part1, part2, sigh, result);
                break;

            case '-':
                result = +part1 - +part2;
                $('#screen').val(result);
                strArr = [];
                addResultField(part1, part2, sigh, result);
                break;

            case '+':
                result = +part1 + +part2;
                $('#screen').val(result);
                strArr = [];
                addResultField(part1, part2, sigh, result);
                break;
        }
    }
});