//write your code here
class MagazineStatus {
    constructor(name, nextStatus) {
        this.name = name;
        this.NextStatus = nextStatus;
    }

    next() {
        return new this.NextStatus();
    }
}

class ReadyForPushNotification extends MagazineStatus {
    constructor() {
        super('readyForPushNotification', ReadyForApprove);
    }
}

class ReadyForApprove extends MagazineStatus {
    constructor() {
        super('readyForApprove', ReadyForPublish);
    }
}

class ReadyForPublish extends MagazineStatus {
    constructor() {
        super('PublishInProgress', PublishInProgress);
    }
}

class PublishInProgress extends MagazineStatus {
    constructor() {
        super('PublishInProgress', PublishInProgress);
    }
}

class MagazineEmployee {
    constructor(name, role, magazine) {
        this.name = name;
        this.role = role;
        this.magazine = magazine;
        this.magazine.staff.push(this);
    }

    addArticle(article) {
        this.article = article;
        if (this.role !== 'manager') {
            this.magazine.articles.push(article);
        }
        if (this.magazine.articles.length >= 5) {
            this.magazine = new Magazine();
            this.magazine.current = this.magazine.states[1];
        }
    }

    approve() {
        if (
            this.magazine.current === 'ReadyForApprove' &&
            this.role === 'manager'
        ) {
            console.log(`Hello ${this.name}. You've approved the 
         changes`);
            this.magazine.current = this.magazine.states[2];
        } else if (this.magazine.articles.length < 5 && this.role === 'manager') {
            console.log(`Hello ${this.name}. You can't approve. We don't 
         have enough of publications`);
        } else if (
            this.magazine.current === 'ReadyForPublish' &&
            this.role === 'manager'
        ) {
            console.log(`Hello ${this.name}. Publications have already 
         been approved by you.`);
        } else if (this.magazine.current === 'PublishInProgress') {
            console.log(`Hello ${this.name}. While we are publishing we 
         can't do any actions`);
        } else {
            console.log('You do not have permissions to do it');
        }
    }

    publish() {
        if (this.magazine.current === 'ReadyForPublish') {
            console.log(`Hello ${this.name}. You've recently published 
        publications.`);
            this.magazine.current = this.magazine.states[3];
            setTimeout(() => {
                this.magazine.current = this.magazine.states[0];
                console.log(this.magazine);
            }, 60000);
        } else if (this.magazine.current === 'PublishInProgress') {
            console.log(`Hello ${this.name}. While we are publishing we 
        can't do any actions`);
        }
    }
}

class Follower {
    constructor(name) {
        this.name = name;
    }

    subscribeTo(magazine, topic) {
        this.magazine = magazine;
        this.topic = topic;
        this.magazine.followers.push(this);
    }

    unsubscribe() {
        const index = this.magazine.followers.indexOf(this);
        if (index !== -1) {
            this.magazine.followers.splice(index, 1);
        }
    }
}


class Magazine {
    constructor() {
        this.states = [
            'ReadyForPushNotification',
            'ReadyForApprove',
            'ReadyForPublish',
            'PublishInProgress'
        ];
        this.articles = [];
        this.followers = [];
        this.staff = [];
        this.current = this.states[0];
    }

    nextState() {
        this.state = this.state.next();
    }
}